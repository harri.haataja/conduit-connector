# Conduit connector

Connector for 20 mm electrical conduit (plastic tube).

* ID   18 mm
* OD   20 mm
* Wall  1 mm

